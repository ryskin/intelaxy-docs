# Кодстаил

В качестве линтера мы используем [Standard](https://standardjs.com/).
Существует под все основные редакторы. Настройки по умолчанию.

### Общие моменты

Импорт параметров следует указывать в столбик

```js
  import {
    View,
    ScrollView,
    TouchableOpacity,
    LayoutAnimation,
    Text,
    InteractionManager
  } from 'react-native'
```

Сначала идет импорт глобальных модулей, после — локальных

```js
  import { connect } from 'react-redux'
  import { withNetworkConnectivity } from 'react-native-offline'
  import { withNavigationFocus } from 'react-navigation-is-focused-hoc'

  import {
    Colors,
    Metrics,
    rem
  } from '../Themes'

  import WorkoutActions from '../Redux/WorkoutRedux'
  import StatusActions from '../Redux/StatusRedux'
```

Сложные обьекты следует всегда раскладывать на части:

```js
  const {
    icon,
    title,
    children,
    expanded
  } = this.props

  const {
    titleDefault,
    titleExpanded,
    titleDone
  } = Styles
```

Названия классов и сложных обьектов, всегда с большой буквы:

```js
  export class DayScreen extends Component {
    ...
  }
```

Названия методов, функций, переменных -- всегда с маленькой и в camelCase

```js
  export function * setStatus (api, { statusType, id, status, args }) {
    const workoutProgram = yield select(store => store.workout.workout)
    let otherArgs = {}
    ...
  }
```

### Стили

Для React Native проектов следует всегда использовать [React Native Extended Stylesheet](https://github.com/vitalets/react-native-extended-stylesheet). Он поддерживает относительные единицы, проценты, а так же много других полезных [штук](https://github.com/vitalets/react-native-extended-stylesheet#features).

В связи с тем, что стандартный функционал RN не предусматривает адаптацию к разным размерам устройств, все размеры элементов следует указывать в относительных единицах или процентах.

#### Относительные единицы

Первым делом нужно настроить библиотеку для относительных единиц.
В отдельном файле прописываем метрику, например:

```js
  import { Dimensions } from 'react-native'
  const width = Dimensions.get('window').width

  let rem = 14

  if (width > 768) {
    rem = 30
  } else if (width > 414) {
    rem = 26
  } else if (width > 375) {
    rem = 18
  } else if (width > 320) {
    rem = 16
  }

  export default rem
```

После, связываем метрику с [React Native Extended Stylesheet](https://github.com/vitalets/react-native-extended-stylesheet). В рут-контейнере указываем:

```js
  import EStyleSheet from 'react-native-extended-stylesheet'
  import rem from '../Themes/Rem'

  EStyleSheet.build({
    $rem: rem
  })
```

Теперь можно использовать метрику в стилях:

```js
  const styles = EStyleSheet.create({
    whatever: {
      width: '5rem',
      paddingVertical: '2rem'
    }
  })
```

Так же, можно использовть в параметрах каких-либо компонентов, просто умножая чисто rem на коэффицент:

```jsx
  <Icon
    name='ios-arrow-back'
    size={rem * 2.3}
    color={Colors.iconOrange} />
```

### Структура проекта

Все имена файлов и папок следует называть с большой буквы в CamelCase.

|![](images/structure1.png "")
|![](images/structure2.png "")|
